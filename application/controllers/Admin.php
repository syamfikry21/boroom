<?php 
	/**
	 * 
	 */
	class Admin extends CI_Controller
	{
		
		function __construct()
		{
			parent::__construct();
			$this->load->helper('log');
			if(!is_logged()) redirect('auth/login_admin'); 
			$this->load->model('room');
			$this->load->model('M_booking');
		}

		public function index(){
			$data['data'] = $this->room->getData();
			$this->load->view('include/admin_head');
			$this->load->view('admin/content_room',$data);
			$this->load->view('include/admin_footer');
		}
			
		public function room_insert(){
			$this->load->view('include/admin_head');
			$this->load->view('admin/content_room_insert');
			$this->load->view('include/admin_footer');
		}

		public function do_insert_room(){
			$kode_ruangan = $this->input->post('kode_ruangan');
			$nama_ruangan = $this->input->post('nama_ruangan');
			$tempat = $this->input->post('tempat');
			$kapasitas = $this->input->post('kapasitas');
			$date = date_create();
			$files=$_FILES;
			$_FILES['userfile']['name']= $files['userfile']['name'];
		    $_FILES['userfile']['type']= $files['userfile']['type'];
		    $_FILES['userfile']['tmp_name']= $files['userfile']['tmp_name'];
		    $_FILES['userfile']['error']= $files['userfile']['error'];
		    $_FILES['userfile']['size']= $files['userfile']['size'];
		    $config['upload_path'] = './uploads/room/';
			$config['allowed_types'] = 'gif|jpg|jpeg|png';
			$this->upload->initialize($config);

            if ( ! $this->upload->do_upload())
            {
                $error = array('error' => $this->upload->display_errors());
				print_r($error);
            }
            else
            {
            	$values = array(
					'id_room' => $kode_ruangan,
					'nama' => $nama_ruangan,
					'tempat' => $tempat,
					'kapasitas' => $kapasitas,
					'gambar' => 'uploads/room/'.$this->upload->data('file_name'),
					'insert_at' => $date->format('Y-m-d H-i-s') 
				);
				$this->room->setValues($values);
                $insert = $this->room->insertData();
				redirect('admin');	
            }
		}
		
		public function edit_room($id){
			$where['id_room'] = $id;
			$this->room->setWhere($where);
			$data['data'] = $this->room->getData()[0];
			$this->load->view('include/admin_head');
			$this->load->view('admin/content_room_edit',$data);
			$this->load->view('include/admin_footer');
		}

		public function do_edit_room($id){
			$nama_ruangan = $this->input->post('nama_ruangan');
			$tempat = $this->input->post('tempat');
			$kapasitas = $this->input->post('kapasitas');
			$date = date_create();
			$values = array(
				'nama' => $nama_ruangan,
				'tempat' => $tempat,
				'kapasitas' => $kapasitas,
				'insert_at' => $date->format('Y-m-d H-i-s') 
			);
			$where['id_room'] = $id;
			$this->room->setValues($values);
			$this->room->setWhere($where);
			$this->room->updateData();
			redirect('admin');
		}

		public function delete_room($id){
			$where['id_room'] = $id;
			$this->room->setValues($values);
			$this->room->setWhere($where);
			$this->room->deleteData();
			redirect('admin');
		}

		public function booking(){
			$this->M_booking->setOrder('id_booking desc');
			$data['data'] = $this->M_booking->getData();
			$this->load->view('include/admin_head');
			$this->load->view('admin/content_booking',$data);
			$this->load->view('include/admin_footer');
		}

		public function detail_booking($id){
			$where['id_booking'] = $id;
			$this->M_booking->setOrder('id_booking desc');
			$this->M_booking->setWhere($where);
			$this->M_booking->activateJoin("user","user.nip = booking.nip");
			$this->M_booking->activateJoin("room","room.id_room = booking.id_room");
			$get = $this->M_booking->getData()[0];
			// echo json_encode($get);
			$data['data'] = $get;
			$this->load->view('include/admin_head');
			$this->load->view('admin/content_detail_booking',$data);
			$this->load->view('include/admin_footer');
		}

		public function setujui($id){
			$where['id_booking'] = $id;
			$value['status'] = 1;
			$this->M_booking->setValues($value);
			$this->M_booking->setWhere($where);
			$update = $this->M_booking->updateData();
			if($update){
				redirect('admin/booking');
			}else{
				echo "Error";
			}
		}

		public function tolak($id){
			$where['id_booking'] = $id;
			$value['status'] = 2;
			$this->M_booking->setValues($value);
			$this->M_booking->setWhere($where);
			$update = $this->M_booking->updateData();
			if($update){
				redirect('admin/booking');
			}else{
				echo "Error";
			}
		}
	}
 ?>