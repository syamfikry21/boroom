<?php 
	/**
	 * 
	 */
	class Auth extends CI_Controller
	{
		
		function __construct()
		{
			parent::__construct();
			$this->load->helper('log');
            $this->load->model('M_admin');
            $this->load->model('M_user');
		}

		public function login_admin(){
			$log = is_logged_user();
        	$data = array(
        		'tittle'=>'Login',
        		'log' => $log
        	);
        	$this->load->view('include/user_header',$data);
			$this->load->view('admin/content_login');
			$this->load->view('include/user_footer');
		}

		public function login_user(){
			$log = is_logged_user();
			$data = array(
        		'tittle'=>'Login',
        		'log' => $log
        	);
        	$this->load->view('include/user_header',$data);
			$this->load->view('body_user/login');
			$this->load->view('include/user_footer');
		}

		public function do_login($idx){
			if($idx == 1){
				$username = $this->input->post('username');
				$password = $this->input->post('password');

				$where = array(
					'username' => $username,
					'password' => $password
				);

				$this->M_admin->setWhere($where);
				$data = $this->M_admin->getData();
				$ct = $this->M_admin->getCount($data);
				if($ct > 0){
					$userdata = array(
						'username' => $username,
						'logged' => true 
						);
					$this->session->set_userdata($userdata);
					redirect('admin');
				}else{
					redirect('auth/login_admin');
				}
			}else if($idx == 2){
				$nip = $this->input->post('username');

				$where = array(
					'nip' => $nip
				);

				$this->M_user->setWhere($where);
				$data = $this->M_user->getData();
				$ct = $this->M_user->getCount($data);
				if($ct > 0){
					$userdata = array(
						'nip' => $nip,
						'logged_user' => true 
						);
					$this->session->set_userdata($userdata);
					redirect('publik');
				}else{
					redirect('auth/login_user');
				}
			}
		}

		public function logout_admin(){
			if(!is_logged()) redirect('auth/login_admin');
			$userdata = array('username'=>'','logged'=>false);
			$this->session->unset_userdata($userdata);
			$this->session->sess_destroy();	
			redirect('auth/login_admin');
		}

		public function logout_user(){
			$userdata = array('nip'=>'','logged_user'=>false);
			$this->session->unset_userdata($userdata);
			$this->session->sess_destroy();	
			redirect('publik');
		}

	}
 ?>