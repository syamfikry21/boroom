<?php
    class Publik extends CI_Controller{

        function __construct(){
            parent::__construct();
            $this->load->helper('log');
            $this->load->model('M_admin');
            $this->load->model('room');
            $this->load->model('M_booking');
        }

        public function index(){
        	$log = is_logged_user();
        	$data = array(
        		'tittle'=>'Jadwal Peminjaman',
                'room' => $this->room->getData(),
                'log' => $log
        	);
        	$this->load->view('include/header',$data);
            $this->load->view('body_user/view_room',$data);
            $this->load->view('include/footer');
        }

        public function detail_room($id){
        	$log = is_logged_user();
        	$where['id_room'] = $id;
        	$this->room->setWhere($where);
            $this->M_booking->setWhere($where);
            $room = $this->room->getData()[0];
            $this->M_booking->setOrder('id_booking desc');
            $booking = $this->M_booking->getData();
        	$data = array(
        		'tittle'=>'Jadwal Peminjaman',
                'room' => $room,
                'booking' => $booking,
                'log' => $log
        	);
        	$this->load->view('include/header',$data);
            $this->load->view('body_user/view_detail_room',$data);
            $this->load->view('include/footer');
        }

        public function booking($id,$nip){
        	if(!is_logged_user()){ 
        		redirect('auth/login_user');
        	}else{
        		$id_room = $id;
        		$nip = $nip;
        		$perihal = $this->input->post('perihal');
        		$tanggal_mulai = $this->input->post('tanggal_mulai');
        		$tanggal_selesai = $this->input->post('tanggal_selesai');
                $files=$_FILES;
                $_FILES['userfile']['name']= $files['userfile']['name'];
                $_FILES['userfile']['type']= $files['userfile']['type'];
                $_FILES['userfile']['tmp_name']= $files['userfile']['tmp_name'];
                $_FILES['userfile']['error']= $files['userfile']['error'];
                $_FILES['userfile']['size']= $files['userfile']['size'];
                $config['upload_path'] = './uploads/nodin/';
                $config['allowed_types'] = 'gif|jpg|jpeg|png|doc|docx|xls|xlsx|ppt|pptx|pdf';
                $this->upload->initialize($config);

                if ( ! $this->upload->do_upload())
                {
                    $error = array('error' => $this->upload->display_errors());
                    print_r($error);
                }
                else
                {
            		$date = date_create();
            		$date1 = new DateTime($tanggal_mulai);
            		$date2 = new DateTime($tanggal_selesai);
            		$value = array(
            			'id_room' => $id_room ,
            			'nip' => $nip,
            			'perihal' => $perihal,
            			'tanggal_mulai' => $date1->format('Y-m-d H:i:s'),
            			'tanggal_selesai' => $date2->format('Y-m-d H:i:s'),
                        'nodin' => 'uploads/nodin/'.$this->upload->data('file_name'),
            			'insert_at' => $date->format('Y-m-d H-i-s') 
            		);

            		$this->M_booking->setValues($value);
            		$insert = $this->M_booking->insertData();
            		if($insert){
            			redirect('publik');
            		}else{
            			echo "Error";
            		}
                }
        	}
        }

        public function transaksi($nip){
        	$log = is_logged_user();
        	$where['nip'] = $nip;
            $this->M_booking->setOrder('id_booking desc');
        	$this->M_booking->setWhere($where);
        	$data = array(
        		'tittle'=>'Jadwal Peminjaman',
                'data' => $this->M_booking->getData(),
                'log' => $log
        	);
        	$this->load->view('include/header',$data);
            $this->load->view('body_user/view_transaksi',$data);
            $this->load->view('include/footer');
        }

        public function getJson(){
            $arr = array();
            $this->M_booking->setField('perihal as title,tanggal_mulai as start,tanggal_selesai as end');
            $data = $this->M_booking->getData();

            echo json_encode($data);
        }

    }
?>