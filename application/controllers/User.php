<?php 
	/**
	 * 
	 */
	class User extends CI_Controller
	{
		function __construct()
		{
			parent::__construct();
			$this->load->helper('log');
			if(!is_logged_user()) redirect('auth/login_user');
			$this->load->model('m_user');
		}

		public function index(){
			$this->load->view('include/user_header');
			$this->load->view('body_user/content_booking_user');
			$this->load->view('include/user_footer');
		}
	}
 ?>