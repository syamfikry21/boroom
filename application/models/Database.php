<?php
//by sugiantooeoen
//www.sugiantooeoen.com
//database model, model ini akan menurunkan ke model yang berhubungan dengan database akan 
class Database extends ci_model{
	public $table;
	protected $where = array();
	protected $like = array();
	protected $order;
	protected $limit;
	protected $uri;
	protected $rand = false;
	protected $field;
	protected $or_where = array();
	protected $groupBy = array();
	public $values = array();
	public function __construct(){
		parent::__construct();
	}

	public function setGroupBy($val){
		$this->groupBy=$val;
	}

	public function setField($field){
		$this->field = $field;
	}
	public function addBatchValue($val){
		$this->values[] = $val;
	}
	public function clearBatchData(){
		$this->values=array();
	}
	public function activateJoin($table,$joinEvt){
		$this->db->join($table, $joinEvt);
	}

	public function setTable($table){
		$this->table = $table;
	}

	public function setOrder($order,$rand = false){
		$this->order = $order;
		$this->rand = $rand;
	}
	public function setWhere($where){
		$this->where= $where;

	}
	public function setOrWhere($where){
		$this->or_where= $where;

	}
	public function setLike($like){
		$this->like = $like;
	}
	public function setLimit($limit){
		$this->limit = $limit;
	}
	public function setUri($uri){
		$this->uri= $uri;
	}

	public function unsetWhere(){
		$this->where= null;
	}
	public function unsetField(){
		$this->field= null;
	}
	public function unsetGroupBy(){
		$this->groupBy= null;
	}
	public function unset_like(){
		$this->like = null;
	}
	public function unset_limit(){
		$this->limit = $limit;
	}
	public function unsetTable(){
		$this->uri= null;
	}

	public function setValues($val){
		$this->values=$val;
	}
	
	public function setWhereAndOr($arrAnd,$arrOr,$operationAnd = "=",$operationOr = "like"){
		$qAnd=array();
		$i=0;
		foreach ($arrAnd as $key => $value) {
			$arKey = explode(" ", $key);
			if(count($arKey)>1){
				$operationAnd = $arKey[1];
				$key = $arKey[0];
			}
			
			$qAnd[$i] = $key ." ".$operationAnd." '".$value."'";
			$i++;
		}
		$qOr;
		$i=0;
		foreach ($arrOr as $key => $value) {
			$arKey = explode(" ", $key);
			
			if(count($arKey)>1){
				$operationOr = $arKey[1];
				$key = $arKey[0];
			}
			$qOr[$i] = $key ." ".$operationOr." '".$value."'";
			$i++;
		}
		$qAnd = implode(" and ", $qAnd);
		$qOr =  implode(" or ", $qOr);
		$this->set_where("(".$qAnd.") and (".$qOr.")");
	}

	public function insertData(){
		$this->db->insert($this->table,$this->values);
		
		return $this->db->insert_id();
	}

	public function insertBatchData(){
		$this->db->insert_batch($this->table,$this->values);
		return true;
	}
	
	public function updateData(){
		$this->db->where($this->where);
		$even = $this->db->update($this->table,$this->values);
		if($even) return true;
		else return false;
	}
	public function getData(){
		if($this->where) $this->db->where($this->where);
		if($this->like) $this->db->or_like($this->like);
		if($this->or_where) $this->db->or_where($this->or_where);
		if($this->field) $this->db->select($this->field);
		if($this->groupBy) $this->db->group_by($this->groupBy);
		if($this->order){
			if(!$this->rand)
				$this->db->order_by($this->order);	
			else
				$this->db->order_by($this->order,"RANDOM");
		} 
		$data = $this->db->get($this->table,$this->limit,$this->uri);
		return $data->result();
	}
	public function getDataAsJson(){
		if($this->where) $this->db->where($this->where);
		if($this->like) $this->db->or_like($this->like);
		if($this->or_where) $this->db->or_where($this->or_where);
		if($this->order) $this->db->order_by($this->order);
		$data = $this->db->get($this->table,$this->limit,$this->uri);
		return json_encode($data->result_array());
	}
	public function deleteData(){
		$even = $this->db->delete($this->table,$this->where);
		if($even) return true;
		else return false;
	}
	public function getCount(){
		if($this->where) $this->db->where($this->where);
		if($this->like) $this->db->or_like($this->like);
		$jml= $this->db->count_all_results($this->table);
		return $jml;	
	}
	public function getDistinct($field){
		$query=$this->db->query("SELECT distinct (".$field.") from `".$this->table."`");
		return $query->result();
	}
	public function clear(){
		$this->db->truncate($this->table); 
	}
}
?>