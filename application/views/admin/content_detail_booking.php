<div class="container">
	<img class="card-img-top" src="<?php echo base_url().$data->gambar; ?>" alt="Card image cap" style="width:500px;height: 367px; margin: 20px 20px 20px 0px;">
	<div class="row">
		<div class="col-12">
			<div class="card" style="margin-bottom: 20px;">
				<div class="card-body">
					<div class="table"> 
						<table class="table table-responsive">
							<thead>
								<th>Nip</th>
								<th>Saker</th>
								<th>Perihal</th>
								<th>Tanggal Mulai</th>
								<th>Tanggal Selesai</th>
								<th>Nodin</th>
								<th>Action</th>
							</thead>
							<tbody>
								<tr>
									<td><?php echo $data->nip ?></td>
									<td><?php echo $data->satker ?></td>
									<td><?php echo $data->perihal ?></td>
									<td><?php echo $data->tanggal_mulai ?></td>
									<td><?php echo $data->tanggal_selesai ?></td>
									<td><a href="<?php echo site_url('/').$data->nodin ?>"><i class="fas fa-file-pdf"></i></a></td>
									<th><a href="<?php echo site_url('admin/setujui/'.$data->id_booking); ?>" class="btn btn-success">Setujui</a> | <a href="<?php echo site_url('admin/tolak/'.$data->id_booking); ?>" class="btn btn-danger">Tolak</a></th>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>