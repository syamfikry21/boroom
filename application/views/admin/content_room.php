				<!-- End of Topbar -->
		        <div class="container">
		        	<div class="col-md-12">
		        		<div class="card shadow mb-4">
				            <div class="card-header py-3">
				              <h6 class="m-0 font-weight-bold text-primary">Data Ruangan</h6>
				            </div>
				            <div class="card-body">
				              <div class="table-responsive">
				              		<div class="text-left"><a href="<?php echo site_url('admin/room_insert'); ?>" class="btn btn-small btn-success">Tambah Ruangan</a></div>
				              		<br>
				              		<table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
					                 	<thead>
					                    	<tr>
					                    		<th>Kode Ruangan</th>
					                      		<th>Nama</th>
					                      		<th>Tempat</th>
					                      		<th>Kapasitas</th>
					                      		<th>Insert at</th>
					                      		<th>Update at</th>
					                      		<th>Action</th>
					                    	</tr>
					                  	</thead>
						                <tbody>
						                	<?php foreach ($data as $key) {
						                	?>
						                    <tr>
						                      <td><?php echo $key->id_room ?></td>
						                      <td><?php echo $key->nama ?></td>
						                      <td><?php echo $key->tempat ?></td>
						                      <td><?php echo $key->kapasitas ?></td>
						                      <td><?php echo $key->insert_at ?></td>
						                      <td><?php echo $key->update_at ?></td>
						                      <td><a href="<?php echo site_url('admin/edit_room/'.$key->id_room); ?>" class="btn btn-small btn-success"><i class="fas fa-edit"></i></a> | <a href="<?php echo site_url('admin/delete_room/'.$key->id_room); ?>" class="btn btn-small btn-danger"><i class="fas fa-trash-alt"></i></a></td>
						                    </tr>
						                	<?php } ?>
					                	</tbody>
					            	</table>
				              </div>
				            </div>
				        </div>
		        	</div>
		        </div> 
			
