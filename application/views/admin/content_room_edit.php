<div class="container">
	<div class="row">
		<div class="col-md-6">
			<div class="card shadow mb-4">
				<div class="card-header py-3">
					<h6 class="m-0 font-weight-bold text-primary">Edit Ruangan</h6>
				</div>
				<div class="card-body">
					<?php echo form_open("admin/do_edit_room/".$data->id_room,""); ?>
					 	<div class="form-group">
					 		<div class="col-md-4">
						    	<label>kode ruangan</label>
					 		</div>
					 		<div class="col-md-12">
					 			<input type="input" class="form-control" id="kode_ruangan" name="kode_ruangan" value="<?php echo $data->id_room ?>" disabled>
					 		</div>
						</div>
						<div class="form-group">
					 		<div class="col-md-4">
						    	<label>Nama Ruangan</label>
					 		</div>
					 		<div class="col-md-12">
					 			<input type="input" class="form-control" id="Nama Ruangan" name="nama_ruangan" value="<?php echo $data->nama ?>">
					 		</div>
						</div>
						<div class="form-group">
					 		<div class="col-md-4">
						    	<label>Tempat</label>
					 		</div>
					 		<div class="col-md-12">
					 			<input type="input" class="form-control" id="tempat" name="tempat" value="<?php echo $data->tempat ?>">
					 		</div>
						</div>
						<div class="form-group">
					 		<div class="col-md-4">
						    	<label>kapasitas</label>
					 		</div>
					 		<div class="col-md-12">
					 			<input type="number" class="form-control" id="kapasitas" name="kapasitas" value="<?php echo $data->kapasitas ?>">
					 		</div>
						</div>
						<div class="form-group">
					 		<input type="submit" name="submit" value="Submit" class="btn btn-small btn-primary">
						</div>
					</form>            
				</div>
			</div>
		</div>
		<div class="col-md-6">
			<div class="card shadow mb-4">
				<div class="card-header py-3">
					<h6 class="m-0 font-weight-bold text-primary">Edit Gambar Ruangan</h6>
				</div>
				<div class="card-body">
					<?php echo form_open("admin/do_edit_room/".$data->id_room,""); ?>
						<div class="form-group">
					 		<img src="<?php echo base_url().$data->gambar; ?>" class="img-fluid" style="width: 440px;height: 274px;">
						</div>
					 	<div class="form-group">
					 		<input type="file" name="userfile" class="form-control">
					 	</div>
					 	<div class="form-group">
					 		<a href="#" class="btn btn-primary">Edit</a>
					 	</div>
					</form>            
				</div>
			</div>
		</div>
	</div>
</div> 