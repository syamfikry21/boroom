<div class="container">
	<div class="col-md-12">
		<div class="card shadow mb-4">
			<div class="card-header py-3">
				<h6 class="m-0 font-weight-bold text-primary">Tambah Ruangan</h6>
			</div>
			<div class="card-body">
				<?php echo form_open_multipart("admin/do_insert_room",""); ?>
				 	<div class="form-group">
				 		<div class="col-md-2">
					    	<label>kode ruangan</label>
				 		</div>
				 		<div class="col-md-6">
				 			<input type="input" class="form-control" id="kode_ruangan" name="kode_ruangan" placeholder="Ex: A001...">
				 		</div>
					</div>
					<div class="form-group">
				 		<div class="col-md-2">
					    	<label>Nama Ruangan</label>
				 		</div>
				 		<div class="col-md-6">
				 			<input type="input" class="form-control" id="Nama Ruangan" name="nama_ruangan" placeholder="Ex: Ruang Conference...">
				 		</div>
					</div>
					<div class="form-group">
				 		<div class="col-md-2">
					    	<label>Tempat</label>
				 		</div>
				 		<div class="col-md-6">
				 			<input type="input" class="form-control" id="tempat" name="tempat" placeholder="Ex: Lantai 3...">
				 		</div>
					</div>
					<div class="form-group">
				 		<div class="col-md-2">
					    	<label>Gambar</label>
				 		</div>
				 		<div class="col-md-6">
				 			<input type="file" class="form-control" id="gambar" name="userfile">
				 		</div>
					</div>
					<div class="form-group">
				 		<div class="col-md-2">
					    	<label>kapasitas</label>
				 		</div>
				 		<div class="col-md-6">
				 			<input type="number" class="form-control" id="kapasitas" name="kapasitas">
				 		</div>
					</div>
					<div class="form-group">
				 		<input type="submit" name="submit" value="Submit" class="btn btn-small btn-primary">
					</div>
				</form>             
			</div>
		</div>
	</div>
</div> 