<div class="uk-container">
	<div class="uk-margin-medium-top">
		<h2 class="uk-align-center">Jadwal Peminjaman Ruangan Kantor BNN Pusat</h2>
		<hr class="uk-divider-icon">
		<table class="uk-table uk-table-hover uk-table-divider">
		    <thead>
		        <tr>
		        	<th> Ruangan </th>
		            <th> Peminjam </th>
		            <th> Satker </th>
		            <th> Perihal </th>
		            <th> Tanggal Mulai</th>
		            <th> Tanggal Selesai</th>
		        </tr>
		    </thead>
		    <tbody>
		        <tr>
		        	<td rowspan="3">Room 1</td>
		            <td>Table Data</td>
		            <td>Table Data</td>
		            <td>Table Data</td>
		            <td>Table Data</td>
		            <td>Table Data</td>
		        </tr>
		        <tr>

		            <td>Table Data</td>
		            <td>Table Data</td>
		            <td>Table Data</td>
		            <td>Table Data</td>
		            <td>Table Data</td>
		        </tr>
		        <tr>
		            <td>Table Data</td>
		            <td>Table Data</td>
		            <td>Table Data</td>
		            <td>Table Data</td>
		            <td>Table Data</td>
		        </tr>
		        <tr>
		        	<td rowspan="3">Room 2</td>
		            <td>Table Data</td>
		            <td>Table Data</td>
		            <td>Table Data</td>
		            <td>Table Data</td>
		            <td>Table Data</td>
		        </tr>
		        <tr>
		            <td>Table Data</td>
		            <td>Table Data</td>
		            <td>Table Data</td>
		            <td>Table Data</td>
		            <td>Table Data</td>
		        </tr>
		        <tr>
		            <td>Table Data</td>
		            <td>Table Data</td>
		            <td>Table Data</td>
		            <td>Table Data</td>
		            <td>Table Data</td>
		        </tr>
		        <tr>
		        	<td rowspan="3">Room 2</td>
		            <td>Table Data</td>
		            <td>Table Data</td>
		            <td>Table Data</td>
		            <td>Table Data</td>
		            <td>Table Data</td>
		        </tr>
		        <tr>
		            <td>Table Data</td>
		            <td>Table Data</td>
		            <td>Table Data</td>
		            <td>Table Data</td>
		            <td>Table Data</td>
		        </tr>
		        <tr>
		            <td>Table Data</td>
		            <td>Table Data</td>
		            <td>Table Data</td>
		            <td>Table Data</td>
		            <td>Table Data</td>
		        </tr>
		    </tbody>
		</table>
	</div>
</div>