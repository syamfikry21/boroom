				<br>
				<div class="container">
					<div class="row">
						<div class="col-6">
							<div class="card">
							  <img class="card-img-top" src="<?php echo base_url().$room->gambar; ?>" alt="Card image cap" style="height: 367px">
							  <div class="card-body">
							  	<div class="row">
							  		<h5 class="text-left"><?php echo $room->nama; ?></h5>
							  	</div>
							  	<div class="row">
							  		<p><?php echo $room->tempat; ?></p>
							  	</div>
							  </div>
							</div>
						</div>
						<div class="col-6">
							<div class="card">
								<div class="card-header">
									<h4>Form Booking</h4>
								</div>
								<div class="card-body">
									<?php echo form_open_multipart('publik/booking/'.$room->id_room.'/'.$this->session->userdata('nip')); ?>
										<div class="form-group">
										<?php if(!$log){?>
								 			<label>NIP</label>
							                <input type="input" name="nip" class="form-control" />
							            <?php }else{?>
							            	<label>NIP</label>
							                <input type="input" name="nip" class="form-control" value="<?php echo $this->session->userdata('nip'); ?>" disabled/>
							            <?php } ?>
							            </div>
							            <div class="form-group">
								 			<label>Perihal</label>
							                <input type="input" name="perihal" class="form-control" />
							            </div>
								 		<div class="form-group">
								 			<label>Start Date</label>
							                <input id="start" width="312" name="tanggal_mulai" />
							            </div>
							            <div class="form-group">
								 			<label>End Date</label>
							                <input id="end" width="312" name="tanggal_selesai"/>
							            </div>
							            <div class="form-group">
								 			<label>Nodin</label>
							                <input type="file" id="userfile" name="userfile" class="form-control" />
							            </div>
							            <div class="form-group">
							                <input type="submit" class="btn btn-primary" value="Submit" />
							            </div>
								 	</form>
								</div>
							</div>
						</div>
					</div>
					<br>
					<div class="row">
						<div class="col-md-12">
			        		<div class="card shadow mb-4">
					            <div class="card-header py-3">
					              <h6 class="m-0 font-weight-bold text-primary">Daftar Booking</h6>
					            </div>
					            <div class="card-body">
					                <div class="table-responsive">
					              		<table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
						                 	<thead>
						                    	<tr>
						                    		<th>Nomor Booking</th>
						                      		<th>Perihal</th>
						                      		<th>Tanggal Mulai</th>
						                      		<th>Tanggal Selesai</th>
						                    	</tr>
						                  	</thead>
							                <tbody>
							                	<?php foreach ($booking as $key) {
							                	?>
							                    <tr>
							                      <td><?php echo $key->id_booking ?></td>
							                      <td><?php echo $key->perihal ?></td>
							                      <td><?php echo $key->tanggal_mulai ?></td>
							                      <td><?php echo $key->tanggal_selesai ?></td>
							                    </tr>
							                	<?php } ?>
						                	</tbody>
						            	</table>
					                </div>
					            </div>
					        </div>
			        	</div>
					</div>
				</div>
				<script>
					$(document).ready(function () {
					  $("#start").datetimepicker({ footer: true, modal: true });
					  $("#end").datetimepicker({ footer: true, modal: true });
					});
				</script>