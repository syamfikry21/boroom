<style type="text/css">
	#script-warning {
	    display: none;
	    background: #eee;
	    border-bottom: 1px solid #ddd;
	    padding: 0 10px;
	    line-height: 40px;
	    text-align: center;
	    font-weight: bold;
	    font-size: 12px;
	    color: red;
	  }

	  #loading {
	    display: none;
	    position: absolute;
	    top: 10px;
	    right: 10px;
	  }

	  #calendar {
	    max-width: 900px;
	    margin: 40px auto;
	    padding: 0 10px;
	  }

	  .fc-title{
	  	color: white;
	  }

	  .fc-time{
	  	color: white;
	  }
</style>
				<br>
				<div class="container">
					<div id='script-warning'>
					    <code>php/get-events.php</code> must be running.
					</div>

					<div id='loading'>loading...</div>

					<div id='calendar'></div>
				</div>
				<br>
				<div>
					<h3 class="text-center">Pilih ruangan yang tersedia</h3><br>
				</div>
				<div class="container">
					<div class="row">
						<?php foreach ($room as $key) {
						?>
						<div class="col-md-4" style="margin-bottom: 20px;">
							<div class="card" style="width: 18rem;">
							  <img class="card-img-top" src="<?php echo base_url().$key->gambar; ?>" alt="Card image cap" style="width:285px; height: 190px;">
							  <div class="card-body">
							  	<div class="row">
							  		<h5 class="text-left"><?php echo $key->nama; ?></h5>
							  	</div>
							  	<div class="row">
							  		<div class="col-md-4"><p class="text-left"><i class="fas fa-users"></i> <?php echo $key->kapasitas; ?></p></div>
							  		<div class="col-md-8 text-right"><a href="<?php echo site_url('publik/detail_room/'.$key->id_room); ?>" class="btn btn-outline-dark">Detail</a></div>
							  	</div>
							  </div>
							</div>
						</div>
						<?php } ?>
					</div>
				</div>
<script>
	var today = new Date();
	var date = today.getFullYear()+'-0'+(today.getMonth()+1)+'-'+today.getDate();
	document.addEventListener('DOMContentLoaded', function() {
	    var calendarEl = document.getElementById('calendar');
	    var calendar = new FullCalendar.Calendar(calendarEl, {
	      plugins: [ 'interaction', 'dayGrid', 'timeGrid', 'list' ],
	      header: {
	        left: 'prev,next today',
	        center: 'title',
	        right: 'dayGridMonth,listWeek'
	      },
	      defaultDate: date,
	      editable: true,
	      navLinks: true, // can click day/week names to navigate views
	      eventLimit: true, // allow "more" link when too many events
	      events: {
	        url: '<?php echo site_url('publik/getJson') ?>',
	        failure: function() {
	          document.getElementById('script-warning').style.display = 'block'
	        }
	      },
	      loading: function(bool) {
	        document.getElementById('loading').style.display =
	          bool ? 'block' : 'none';
	      }
	    });

	    calendar.render();
	});
</script>