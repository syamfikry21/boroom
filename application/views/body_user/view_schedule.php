				<nav class="navbar" style="background-color:#0e698f;color:#FFF;border-bottom: solid 1px #ccc;">
				  <a class="navbar-brand"><h2>BoRoom</h2></a>
				  <a href="<?php echo site_url('auth/login_user'); ?>" class="btn btn-outline-light">Login</a>
				</nav>
				<br>
				<div>
					<h3 class="text-center">Pilih ruangan yang tersedia</h3><br>
				</div>
				<div class="container">
					<div class="row">
						<?php foreach ($room as $key) {
						?>
						<div class="col-md-4" style="margin-bottom: 20px;">
							<div class="card" style="width: 18rem;">
							  <img class="card-img-top" src="<?php echo base_url().$key->gambar; ?>" alt="Card image cap" style="width:285px; height: 190px;">
							  <div class="card-body">
							  	<div class="row">
							  		<h5 class="text-left"><?php echo $key->nama; ?></h5>
							  	</div>
							  	<div class="row">
							  		<div class="col-md-4"><p class="text-left"><i class="fas fa-users"></i> <?php echo $key->kapasitas; ?></p></div>
							  		<div class="col-md-8 text-right"><a href="#" class="btn btn-outline-dark">Detail</a></div>
							  	</div>
							  </div>
							</div>
						</div>
						<?php } ?>
						<div class="col-md-4" style="margin-bottom: 20px;">
							<div class="card" style="width: 18rem;">
							  <img class="card-img-top" src="https://www.thebreeze.biz/wp-content/uploads/2018/04/Claremont-House-11.jpg" alt="Card image cap" style="width:285px; height: 190px;">
							  <div class="card-body">
							    <div class="row">
							  		<h5 class="text-left">Ruang Meeting A</h5>
							  	</div>
							  	<div class="row">
							  		<div class="col-md-4"><p class="text-left"><i class="fas fa-users"></i> 50</p></div>
							  		<div class="col-md-8 text-right"><a href="#" class="btn btn-outline-dark">Detail</a></div>
							  	</div>
							  </div>
							</div>
						</div>
						<div class="col-md-4" style="margin-bottom: 20px;">
							<div class="card" style="width: 18rem;">
							  <img class="card-img-top" src="https://www.thebreeze.biz/wp-content/uploads/2018/04/Bartle-House-61.jpg" alt="Card image cap" style="width:285px; height: 190px;">
							  <div class="card-body">
							    <div class="row">
							  		<h5 class="text-left">Ruang Meeting A</h5>
							  	</div>
							  	<div class="row">
							  		<div class="col-md-4"><p class="text-left"><i class="fas fa-users"></i> 50</p></div>
							  		<div class="col-md-8 text-right"><a href="#" class="btn btn-outline-dark">Detail</a></div>
							  	</div>
							  </div>
							</div>
						</div>
						<div class="col-md-4" style="margin-bottom: 20px;">
							<div class="card" style="width: 18rem;">
							  <img class="card-img-top" src="https://www.thebreeze.biz/wp-content/uploads/2018/04/Claremont-House-Light-and-Bright-meeting-rooms-combined-4-Edit.jpg" alt="Card image cap" style="width:285px; height: 190px;" style="width:285px; height: 190px;">
							  <div class="card-body">
							   <div class="row">
							  		<h5 class="text-left">Ruang Meeting A</h5>
							  	</div>
							  	<div class="row">
							  		<div class="col-md-4"><p class="text-left"><i class="fas fa-users"></i> 50</p></div>
							  		<div class="col-md-8 text-right"><a href="#" class="btn btn-outline-dark">Detail</a></div>
							  	</div>
							  </div>
							</div>
						</div>
					</div>
				</div>
				<!-- <div class="container">
					<br>
		        	<div class="col-md-12">
		        		<div class="card shadow mb-4">
				            <div class="card-header py-3">
				              <div class="text-right">
				              		<form class="form-inline my-2 my-lg-0">
								      <input type="date" class="form-control mr-sm-2" id="start_time" name="start_time">
								      <input type="date" class="form-control mr-sm-2" id="end_time" name="end_time">
								      <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Submit</button>
								    </form>
				              </div>  
				            </div>
				            <div class="card-body">
				                <div class="table-responsive">
				                	<table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
				                		<thead>
					                    	<tr>
					                    		<th>Tanggal</th>
					                      		<th>Jam Mulai</th>
					                      		<th>Jam Selesai</th>
					                      		<th>Acara</th>
					                      		<th>Tanggal Mulai</th>
					                      		<th>Unit Kerja</th>
					                    	</tr>
					                  	</thead>
						                <tbody>
						                    <tr>
						                      <td></td>
						                      <td></td>
						                      <td></td>
						                      <td></td>
						                      <td></td>
						                      <td></td>
						                    </tr>
					                	</tbody>
				                	</table>	
				                </div>
				            </div>
				        </div>
		        	</div>
		        </div> --> 