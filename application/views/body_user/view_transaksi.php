				<br>
				<div class="container">
		        	<div class="col-md-12">
		        		<div class="card shadow mb-4">
				            <div class="card-header py-3">
				              <h6 class="m-0 font-weight-bold text-primary">Data Transaksi</h6>
				            </div>
				            <div class="card-body">
				                <div class="table-responsive">
				              		<table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
					                 	<thead>
					                    	<tr>
					                    		<th>Nomor Booking</th>
					                      		<th>Kode Ruangan</th>
					                      		<th>Perihal</th>
					                      		<th>Tanggal Mulai</th>
					                      		<th>Tanggal Selesai</th>
					                      		<th>Status</th>
					                      		<th>Action</th>
					                    	</tr>
					                  	</thead>
						                <tbody>
						                	<?php foreach ($data as $key) {
						                	?>
						                    <tr>
						                      <td><?php echo $key->id_booking ?></td>
						                      <td><?php echo $key->id_room ?></td>
						                      <td><?php echo $key->perihal ?></td>
						                      <td><?php echo $key->tanggal_mulai ?></td>
						                      <td><?php echo $key->tanggal_selesai ?></td>
						                      <td><?php 
						                      	if($key->status == 0){
						                      		echo "Menunggu persetujuan";
						                      	}else if($key->status == 1){
						                      		echo "Disejui";
						                      	}else{
						                      		echo "Tidak Disejui";
						                      	} ?>
						                      	</td>
						                      <td><a href="#" class="btn btn-small btn-success"><i class="fas fa-eye"></i></a> | <a href="#" class="btn btn-small btn-danger"><i class="fas fa-trash-alt"></i></a></td>
						                    </tr>
						                	<?php } ?>
					                	</tbody>
					            	</table>
				                </div>
				            </div>
				        </div>
		        	</div>
		        </div> 