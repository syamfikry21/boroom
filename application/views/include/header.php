<!DOCTYPE html>
<html>
<head>
	<title><?php echo $tittle; ?></title>
	<!-- UIkit CSS -->
	<link href="<?php echo base_url('sources/vendor/fontawesome-free/css/all.min.css'); ?>" rel="stylesheet" type="text/css">
  	<link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

  <!-- Custom styles for this template-->
  	<link href="<?php echo base_url('sources/css/sb-admin-2.min.css'); ?>" rel="stylesheet">
  	<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
    <link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url('sources/packages/core/main.css'); ?>" rel='stylesheet' />
	<link href="<?php echo base_url('sources/packages/daygrid/main.css'); ?>" rel='stylesheet' />
	<link href="<?php echo base_url('sources/packages/timegrid/main.css'); ?>" rel='stylesheet' />
	<link href="<?php echo base_url('sources/packages/list/main.css'); ?>" rel='stylesheet' />
	<script src="<?php echo base_url('sources/packages/core/main.js'); ?>"></script>
	<script src="<?php echo base_url('sources/packages/interaction/main.js'); ?>"></script>
	<script src="<?php echo base_url('sources/packages/daygrid/main.js'); ?>"></script>
	<script src="<?php echo base_url('sources/packages/timegrid/main.js'); ?>"></script>
	<script src="<?php echo base_url('sources/packages/list/main.js'); ?>"></script>
</head>
<body>
	<nav class="navbar" style="background-color:#4e73df;color:#FFF;border-bottom: solid 1px #ccc;">
		<a class="navbar-brand"><h2>BoRoom</h2></a>
		<?php if(!$log){?>
			<a href="<?php echo site_url('auth/login_user'); ?>" class="btn btn-outline-light">Login</a>
		<?php }else{ ?>
			<div class="btn-group">
			  <button type="button" class="btn btn-outline-light dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
			    <?php echo $this->session->userdata('nip'); ?>
			  </button>
			  <div class="dropdown-menu">
			  	<a class="dropdown-item" href="<?php echo site_url('publik'); ?>">Home</a>
			    <a class="dropdown-item" href="<?php echo site_url('publik/transaksi/'.$this->session->userdata('nip')); ?>">Transaksi</a>
			    <div class="dropdown-divider"></div>
			    <a class="dropdown-item" href="<?php echo site_url('auth/logout_user') ?>">logout</a>
			  </div>
			</div>
		<?php } ?>
	</nav>
