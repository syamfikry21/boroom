-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Sep 15, 2019 at 07:45 PM
-- Server version: 10.1.19-MariaDB
-- PHP Version: 5.6.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_boroom`
--

-- --------------------------------------------------------

--
-- Table structure for table `room`
--

CREATE TABLE `room` (
  `id_room` char(4) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `tempat` varchar(25) NOT NULL,
  `kapasitas` int(11) NOT NULL,
  `gambar` text NOT NULL,
  `insert_at` datetime NOT NULL,
  `update_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `room`
--

INSERT INTO `room` (`id_room`, `nama`, `tempat`, `kapasitas`, `gambar`, `insert_at`, `update_at`) VALUES
('A009', 'Ruangan Meeting 12A', 'Lantai 3', 50, 'uploads/room/Claremont-House-92.jpg', '2019-09-15 19:12:38', '2019-09-15 17:12:38'),
('A010', 'Ruangan Meeting 13A', 'Lantai 3', 100, 'uploads/room/Claremont-House-11.jpg', '2019-09-15 19:19:55', '2019-09-15 17:19:55'),
('A011', 'Ruangan Meeting 13B', 'Lantai 4', 150, 'uploads/room/Bartle-House-61.jpg', '2019-09-15 19:20:29', '2019-09-15 17:20:30');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `room`
--
ALTER TABLE `room`
  ADD PRIMARY KEY (`id_room`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
